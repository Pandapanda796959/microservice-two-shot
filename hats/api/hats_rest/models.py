from django.db import models


class LocationsVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=200)
    section_number = models.PositiveSmallIntegerField()
    shelf_number = models.PositiveSmallIntegerField()


class Hats(models.Model):
    name = models.CharField(max_length=100, null=True)
    fabric = models.CharField(max_length=100, null=True)
    style_name = models.CharField(max_length=100, null=True)
    color = models.CharField(max_length=100, null=True)
    picture_url = models.URLField(null=True)

    location = models.ForeignKey(
        LocationsVO, related_name="hats", on_delete=models.CASCADE,
    )

    def __str_(self):
        return self.name

    class Meta:
        ordering = ("name",)
