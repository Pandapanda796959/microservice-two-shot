from django.urls import path

from .views import api_hats_detail, api_hats_list, api_locations

urlpatterns = [
    path("hats/", api_hats_list, name="api_hats_list"),
    path(
        "hats/<int:pk>/",
        api_hats_detail,
        name="api_hats_detail",
    ),
    path("locations/", api_locations, name="api_locations")
]
