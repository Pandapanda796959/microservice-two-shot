from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json
from .models import Hats, LocationsVO
from common.json import ModelEncoder


class LocationsEncoder(ModelEncoder):
    model = LocationsVO
    properties = [
        "import_href",
        "closet_name",
        "section_number",
        "shelf_number",
    ]


class HatsEncoder(ModelEncoder):
    model = Hats
    properties = [
        "location",
        "name",
        "fabric",
        "style_name",
        "color",
        "id",
    ]
    encoders = {
        "location": LocationsEncoder()
    }


@require_http_methods(["GET", "POST"])
def api_hats_list(request):
    if request.method == "GET":
        hats = Hats.objects.all()
        return JsonResponse({"hats": hats}, encoder=HatsEncoder)
    else:
        content = json.loads(request.body)
        try:
            location_href = content["location"]
            location = LocationsVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationsVO.DoesNotExist:
            return JsonResponse(
                {"message": "location not found"},
                status=400,
            )

        hats = Hats.objects.create(**content)
        return JsonResponse({"hats": hats}, encoder=HatsEncoder, safe=False)




@require_http_methods(["GET", "PUT", "DELETE"])
def api_hats_detail(request, pk):
    if request.method == "GET":
        hat = Hats.objects.get(id=pk)
        return JsonResponse(hat, encoder=HatsEncoder, safe=False)
    elif request.method == "DELETE":
        count, _ = Hats.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        hat = Hats.objects.filter(id=pk).update(**content)
        return JsonResponse({"hat": hat}, encoder=HatsEncoder, safe=False)


@require_http_methods(["GET"])
def api_locations(request):
    if request.method == "GET":
        locations = LocationsVO.objects.all()
        return JsonResponse(
            {"locations": locations}, encoder=LocationsEncoder, safe=False
        )
