from django.shortcuts import render
from .models import Shoe, BinVO
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from common.json import ModelEncoder
import json

class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = [
       "import_href",
       "closet_name"
    ]


class ShoesListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "id",
        "manufacturer",
        "model_name",
        "color",
        "picture_URL",
        "bin",
    ]
    encoders = { "bin": BinVOEncoder()}


@require_http_methods(["POST", "GET"])
def api_list_shoes(request):
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoesListEncoder,
            safe=False
        )
    elif request.method == "POST":
        content = json.loads(request.body)
        try:
            bin_href = content["bin"]
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "invalid bin id"},
                status=400
            )
        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            {"shoe": shoe},
            encoder=ShoesListEncoder,
            safe=False
        )


@require_http_methods(["GET", "DELETE"])
def api_show_shoe(request, id):
    if request.method == "GET":
        shoe = Shoe.objects.get(id=id)
        return JsonResponse(
            {"shoe": shoe},
            encoder=ShoesListEncoder
        )
    elif request.method == "DELETE":
        count, _ = Shoe.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})


def show_BinVO(request):
    if request.method == "GET":
        bins = BinVO.objects.all()
        return JsonResponse(
            {"bins": bins},
            encoder=BinVOEncoder,
            safe=False
        )
