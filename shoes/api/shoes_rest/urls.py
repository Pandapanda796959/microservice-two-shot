from django.urls import path
from .views import api_list_shoes, api_show_shoe, show_BinVO

urlpatterns = [
    path("shoes/", api_list_shoes, name="api_list_shoes"),
    path("shoes/<int:id>/", api_show_shoe, name="api_show_shoe"),
    path("bins/", show_BinVO)
]
