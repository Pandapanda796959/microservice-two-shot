import React, { useEffect, useState } from 'react'

function HatsList() {
    const [hats, setHats] = useState([])
    const getData = async () => {
        const response = await fetch('http://localhost:8090/api/hats/')
        if(response.ok) {
            const data = await response.json()
            setHats(data.hats)
        }
    }

    useEffect(() => {
        getData()
    }, [])

    const deleteHat = async (hat) => {
      const url = `http://localhost:8090/api/hats/${hat}/`;
      const fetchConfig = {method: "delete"};
      await fetch(url, fetchConfig);
      window.location.reload();
  }

    return(
        <>
        <div className="col col-sm-auto">
        <img width="300" className="bg-white rounded shadow d-block mx-auto mb-4" />
      </div>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Fabric</th>
              <th>Style</th>
              <th>Color</th>
              <th>Closet Name</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
          {hats.map(hat => {
            return (
              <tr key={ hat.id }>
              <td>{ hat.id }</td>
              <td>{ hat.name }</td>
              <td>{ hat.fabric }</td>
              <td>{ hat.style_name }</td>
              <td>{ hat.color }</td>
              <td>{ hat.location.closet_name }</td>
              <td>
                <button onClick={() => deleteHat(hat.id)}>Delete</button>
              </td>
              </tr>
            )
          })}
          </tbody>
        </table>
        </>
    )

}

export default HatsList
