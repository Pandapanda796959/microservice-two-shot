import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import ShoeList from './ShoeList';
import ShoeForm from './ShoeForm';
import Nav from './Nav';
import HatsList from './HatsList'
import HatsForm from './HatsForm'



function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route index element={<MainPage />} />
          <Route path="/shoes">
            <Route index element={<ShoeList />} />
            <Route path="/shoes/new" element={<ShoeForm/>} />
          </Route>
          <Route path="/hats">
            <Route index element= {<HatsList />} />
            <Route path="/hats/new" element= {<HatsForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
