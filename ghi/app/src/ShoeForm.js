import { useState, useEffect } from "react";

export default function ShoeForm() {
    const [bins, setBins] = useState([]);
    const [formData, setFormData] = useState({
        manufacturer: '',
        model_name: '',
        color:'',
        picture_URL: '',
        bin: '',
    });

    const fetchBins = async () => {
        const url = "http://localhost:8100/api/bins/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setBins(data.bins);
        } else {
            console.log("error fetching data");
        }
    }

    useEffect(() =>{
        fetchBins();
    },[])



    const handleSubmit = async (event) => {
        event.preventDefault();
        const url = "http://localhost:8080/api/shoes/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                "Content-Type": "application/json"
            }
        }
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            setFormData({
                manufacturer: '',
                model_name: '',
                color: '',
                picture_URL: '',
                bin: '',
            });
        }
    }

    const handleFormChange = (event) => {
            const value = event.target.value;
            const inputName = event.target.name;
            setFormData({
                ...formData,
                [inputName]: value
            });
        }

    return (
        <div>
            <div>
            <h1>Add a shoe</h1>
                <form onSubmit={handleSubmit}>
                    <div>
                        <label htmlFor="manufacturer">Manufacturer</label>
                        <input onChange={handleFormChange} placeholder="Manufacturer" name="manufacturer" required type="text" className="form-control" id="manufacturer" value={formData.manufacturer} />
                    </div>
                    <div>
                        <label htmlFor="model_name">Model name</label>
                        <input onChange={handleFormChange} name="model_name" placeholder="Model name" required type="text" className="form-control" id="model_name" value={formData.model_name} />
                    </div>
                    <div>
                        <label htmlFor="color">Color</label>
                        <input onChange={handleFormChange} name="color" placeholder="Color" required type="text" className="form-control" id="color" value={formData.color} />
                    </div>
                    <div>
                        <label htmlFor="image_URL">Image URL</label>
                        <input onChange={handleFormChange} name="picture_URL" placeholder="Picture URL" required type="url" className="form-control" id="picture_URL" value={formData.picture_URL}  />
                    </div>
                    <div>
                        <select onChange={handleFormChange} name="bin" id="bin" required className="form-control" >
                            <option name="bin" value="">Choose a bin</option>
                            { bins.map((bin) => {
                                return(
                                    <option value={bin.href} key={bin.id}>{bin.closet_name}</option>
                                );
                            })}
                        </select>
                    </div>
                    <button>Create</button>
                </form>
            </div>
        </div>
    );
}
