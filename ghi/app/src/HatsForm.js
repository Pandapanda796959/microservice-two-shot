import React, { useState, useEffect } from 'react';

function HatsForm() {
    // const[hats, setHats] = useState([])

    const [formData, setFormData] = useState({
        name: '',
        fabric: '',
        style_name: '',
        color: '',
        location: '',
    })

    const [locations, setLocations] = useState([])
    const fetchData = async () => {
      const url = 'http://localhost:8100/api/locations'
      const response = await fetch(url)

      if (response.ok) {
        const data = await response.json()
        setLocations(data.locations)
      }
    }
    useEffect(() => {
      fetchData()
    }, [])

    const handleSubmit = async (event) => {
        event.preventDefault()
        const url = 'http://localhost:8090/api/hats/'

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const response = await fetch(url, fetchConfig)

        if (response.ok) {
            setFormData({
                name: '',
                fabric: '',
                style_name: '',
                color: '',
                location: '',
            })
        }
    }
    const handleFormChange = (e) => {
        const value = e.target.value
        const inputName = e.target.name

        setFormData({
            ...formData,
            [inputName]: value
        })
    }
    return(
        <div className="row">
        <div className="col col-sm-auto">
          <img width="300" className="bg-white rounded shadow d-block mx-auto mb-4" />
        </div>
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add Hat</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={formData.name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={formData.fabric} placeholder="Fabric Type" required type="text" name="fabric" id="fabric" className="form-control" />
                <label htmlFor="fabric">Fabric</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={formData.style_name} placeholder="Style" required type="text" name="style_name" id="style_name" className="form-control"  />
                <label htmlFor="style_name">Style</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={formData.color} placeholder="Color" required type="text" id="color" name="color" className="form-control"  />
                <label htmlFor="color">Color</label>
              </div>
              <div className="mb-3">
              <select onChange={handleFormChange} value={formData.location}  name="location" id="location"  className="form-select">
                <option value="">Choose a location</option>
                {locations.map((location) => {
                  return (
                    <option key={location.href} value={location.href}>{location.closet_name}</option>
                  );
                })}
              </select>
            </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )

}

export default HatsForm
