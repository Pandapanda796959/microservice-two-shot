import { useState, useEffect } from "react";

export default function ShoeList() {
    const [shoes, setShoes] = useState([]);
    const LoadShoes = async () => {
        const url = "http://localhost:8080/api/shoes/"
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            console.log(data.shoes.bin)
            setShoes(data.shoes)
        } else {
            console.error(response);
        }
    }

    useEffect(() => {
        LoadShoes();
    }, []);

    const deleteShoe = async (shoe) => {
        const url = `http://localhost:8080/api/shoes/${shoe}/`;
        const fetchConfig = {method: "delete"};
        await fetch(url, fetchConfig);
        window.location.reload();
    }

    return (
        <>
            <h1>Shoes</h1>
            <div>
                {shoes.map((shoe) => {
                    return (
                        <div className="card" key={shoe.id}>
                            <table className="table">
                                <thead>
                                    <tr>
                                        <th>Manufacturer</th>
                                        <th>Model Name</th>
                                        <th>Color</th>
                                        <th>Bin</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>{shoe.manufacturer}</td>
                                        <td>{shoe.model_name}</td>
                                        <td>{shoe.color}</td>
                                        <td>{shoe.bin.closet_name}</td>
                                    </tr>
                                </tbody>
                            </table>
                            <div>
                                <img src={`${shoe.image_URL}/`} className="img-thumbnail" />
                            </div>
                            <div>
                                <button onClick={() => deleteShoe(shoe.id)}>Delete</button>
                            </div>
                            <p></p>
                        </div>

                    );
                })
                }
            </div>

        </>
    );
}
